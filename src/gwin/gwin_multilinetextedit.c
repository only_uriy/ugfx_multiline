/*
 * This file is subject to the terms of the GFX License. If a copy of
 * the license was not distributed with this file, you can obtain one at:
 *
 *              http://ugfx.io/license.html
 */

/**
 * @file	src/gwin/gwin_multilinetextedit.c
 * @brief	GWIN MultiLineTextEdit widget header file
 */

#include "../../gfx.h"

#if GFX_USE_GWIN && GWIN_NEED_MULTILINETEXTEDIT

#include "gwin_class.h"
#include <string.h>

// Some settings
#define TEXT_PADDING_LEFT		4
#define CURSOR_PADDING_LEFT		0
#define CURSOR_EXTRA_HEIGHT		1

// Macros to assist in data type conversions
#define gh2obj ((GMultiLineTexteditObject *)gh)
#define gw2obj ((GMultiLineTexteditObject *)gw)

static void MultiLineTextEditRemoveChar(GHandle gh) {
	char		*p;
	const char	*q;
	unsigned	sz;
	unsigned	pos;

	sz = strlen(gh2obj->w.text);
	pos = gh2obj->cursorPos;
	if (pos > sz)
		pos = gh2obj->cursorPos = sz;
	q = gh2obj->w.text+pos;
	
	if (!(gh->flags & GWIN_FLG_ALLOCTXT)) {
		// Allocate and then copy
		if (!(p = gfxAlloc(sz)))
			return;
		if (pos)
			memcpy(p, gh2obj->w.text, pos);
		memcpy(p+pos, q+1, sz-pos);
		gh->flags |= GWIN_FLG_ALLOCTXT;
	} else {
		// Copy and then reallocate
		memcpy((char *)q, q+1, sz-pos);
		if (!(p = gfxRealloc((char *)gh2obj->w.text, sz+1, sz)))		// This should never fail as we are making it smaller
			return;
	}
	gh2obj->w.text = p;
}

static gBool MultiLineTextEditAddChars(GHandle gh, unsigned cnt) {
	char		*p;
	const char	*q;
	unsigned	sz;
	unsigned	pos;

	// Get the size of the text buffer
	sz = strlen(gh2obj->w.text)+1;
	pos = gh2obj->cursorPos;
	if (pos >= sz)
		pos = gh2obj->cursorPos = sz-1;

	if (!(gh->flags & GWIN_FLG_ALLOCTXT)) {
		if (!(p = gfxAlloc(sz+cnt)))
			return gFalse;
		memcpy(p, gh2obj->w.text, pos);
		memcpy(p+pos+cnt, gh2obj->w.text+pos, sz-pos);
		gh->flags |= GWIN_FLG_ALLOCTXT;
		gh2obj->w.text = p;
	} else {
		if (!(p = gfxRealloc((char *)gh2obj->w.text, sz, sz+cnt)))
			return gFalse;
		gh2obj->w.text = p;
		q = p+pos;
		p += sz;
		while(--p >= q)
			p[cnt] = p[0];
	}
	return gTrue;
}

// Function that allows to set the cursor to any position in the string
// This should be optimized. Currently it is an O(n^2) problem and therefore very
// slow. An optimized version would copy the behavior of mf_get_string_width()
// and do the comparation directly inside of that loop so we only iterate
// the string once.
static void MultiLineTextEditMouseDown(GWidgetObject* gw, gCoord x, gCoord y) {
	gU16 i = 0;

	(void)y;
	char full_str[256];
	char str_one_row[64];
	memset(str_one_row, 0, sizeof(str_one_row));

	gw2obj->cursorPosX = x;
	gw2obj->cursorPosY = y;
	gw2obj->cursorPosRow = y/ONE_ROW_HEIGHT;
	strcpy(full_str, gw->text);

	strncpy(str_one_row, &full_str[CHAR_IN_ROW * gw2obj->cursorPosRow], CHAR_IN_ROW);

	i = 1;
	while (gdispGetStringWidthCount(str_one_row, gw->g.font, i) < x) {
		i++;
	}

	gw2obj->cursorPos = (i-1) + (gw2obj->cursorPosRow) * CHAR_IN_ROW;	
	_gwinUpdate((GHandle)gw);
}

#if (GFX_USE_GINPUT && GINPUT_NEED_KEYBOARD) || GWIN_NEED_KEYBOARD
	static void MultiLineTextEditKeyboard(GWidgetObject* gw, GEventKeyboard* pke) {
		// Only react on KEYDOWN events. Ignore KEYUP events.
		if ((pke->keystate & GKEYSTATE_KEYUP) || !pke->bytecount)
			return;

		// Is it a special key?
		if (pke->keystate & GKEYSTATE_SPECIAL) {
			// Arrow keys to move the cursor
			gwinMultiLineTextEditSendSpecialKey(&gw->g, (gU8)pke->c[0]);
			return;

		}

		gwinMultiLineTextEditSendKey(&gw->g, pke->c, pke->bytecount);
	}
#endif

static const gwidgetVMT multilinetexteditVMT = {
	{
		"MultiLineTextEdit",					// The class name
		sizeof(GMultiLineTexteditObject),		// The object size
		_gwidgetDestroy,						// The destroy routine
		_gwidgetRedraw, 						// The redraw routine
		0,										// The after-clear routine
	},
	gwinMultiLineTexteditDefaultDraw,		// default drawing routine
	#if GINPUT_NEED_MOUSE
		{
			MultiLineTextEditMouseDown,		// Process mouse down events (NOT USED)
			0,						// Process mouse up events (NOT USED)
			0,						// Process mouse move events (NOT USED)
		},
	#endif
	#if (GFX_USE_GINPUT && GINPUT_NEED_KEYBOARD) || GWIN_NEED_KEYBOARD
		{
			MultiLineTextEditKeyboard		// Process keyboard key down events
		},
	#endif
	#if GINPUT_NEED_TOGGLE
		{
			0,						// No toggle role
			0,						// Assign Toggles (NOT USED)
			0,						// Get Toggles (NOT USED)
			0,						// Process toggle off event (NOT USED)
			0,						// Process toggle on event (NOT USED)
		},
	#endif
	#if GINPUT_NEED_DIAL
		{
			0,						// No dial roles
			0,						// Assign Dials (NOT USED)
			0, 						// Get Dials (NOT USED)
			0,						// Procees dial move events (NOT USED)
		},
	#endif
};

GHandle gwinGMultiLineTexteditCreate(GDisplay* g, GMultiLineTexteditObject* wt, GWidgetInit* pInit, gMemSize maxSize)
{
	// Create the underlying widget
	if (!(wt = (GMultiLineTexteditObject*)_gwidgetCreate(g, &wt->w, pInit, &multilinetexteditVMT)))
		return 0;

	wt->maxSize = maxSize;

	// Set cursor position
	wt->cursorPos = 0;

	gwinSetVisible(&wt->w.g, pInit->g.show);

	return (GHandle)wt;
}

#if (GFX_USE_GINPUT && GINPUT_NEED_KEYBOARD) || GWIN_NEED_KEYBOARD
	void gwinMultiLineTextEditSendSpecialKey(GHandle gh, gU8 key) {
		unsigned sz;

 		// Is it a valid handle?
 		if (gh->vmt != (gwinVMT*)&multilinetexteditVMT)
 			return;
 
		// Check that cursor position is within buffer (in case text has been changed)
		sz = strlen(gh2obj->w.text);
		if (gh2obj->cursorPos > sz)
			gh2obj->cursorPos = sz;

		// Arrow keys to move the cursor
		switch (key) {
		case GKEY_LEFT:
			if (!gh2obj->cursorPos)
				return;
			gh2obj->cursorPos--;
			break;
		case GKEY_RIGHT:
			if (!gh2obj->w.text[gh2obj->cursorPos])
				return;
			gh2obj->cursorPos++;
			break;
		case GKEY_HOME:
			if (!gh2obj->cursorPos)
				return;
			gh2obj->cursorPos = 0;
			break;
		case GKEY_END:
			if (!gh2obj->w.text[gh2obj->cursorPos])
				return;
			gh2obj->cursorPos = sz;
			break;
		default:
			return;
		}

		_gwinUpdate(gh);
	}

	void gwinMultiLineTextEditSendKey(GHandle gh, char *key, unsigned len) {
		// Is it a valid handle?
		if (gh->vmt != (gwinVMT*)&multilinetexteditVMT || !key || !len)
			return;

		// Normal key press
		switch((gU8)key[0]) {
		case GKEY_BACKSPACE:
			// Backspace
			if (!gh2obj->cursorPos)
				return;
			gh2obj->cursorPos--;
			MultiLineTextEditRemoveChar(gh);
			break;
		case GKEY_TAB:
		case GKEY_LF:
		case GKEY_CR:
			// Move to the next field
			_gwinMoveFocus();
			return;
		case GKEY_DEL:
			// Delete
			if (!gh2obj->w.text[gh2obj->cursorPos])
				return;
			MultiLineTextEditRemoveChar(gh);
			break;
		default:
			// Ignore any other control characters
			if ((gU8)key[0] < GKEY_SPACE)
				return;

			// Keep the edit length to less than the maximum
			if (gh2obj->maxSize && strlen(gh2obj->w.text)+len > gh2obj->maxSize)
				return;

			// Make space
			if (MultiLineTextEditAddChars(gh, len)) {
				// Insert the characters
				memcpy((char *)gh2obj->w.text+gh2obj->cursorPos, key, len);
				gh2obj->cursorPos += len;
			}
			break;
		}

		_gwinUpdate(gh);
	}
#endif

void gwinMultiLineTexteditDefaultDraw(GWidgetObject* gw, void* param)
{
	const char*			p;
	gCoord				cpos=1, tpos=1, rowpos;
	const GColorSet*	pcol;
	gCoord				char_in_line=0, i;
	char txt[64];
	char full_str[256];
	char str_one_row[64];
	memset(str_one_row, 0, sizeof(str_one_row));

	(void)param;

	// Is it a valid handle?
	if (gw->g.vmt != (gwinVMT*)&multilinetexteditVMT)
		return;

	// Retrieve colors
	if ((gw->g.flags & GWIN_FLG_SYSENABLED))
		pcol = &gw->pstyle->enabled;
	else
		pcol = &gw->pstyle->disabled;

	// Adjust the text position so the cursor fits in the window
	p = gw->text;
	rowpos = gw2obj->cursorPosRow; // here is cursor row position. 0 is top row

/*	
	if (!gw2obj->cursorPos)
		tpos = 0;
	else {
		for(cpos = gw2obj->cursorPos; ; p++, cpos--) {
			tpos = gdispGetStringWidthCount(p, gw->g.font, cpos);
			if (tpos < gw->g.width)
				break;
		}
	}
*/

	char_in_line = CHAR_IN_ROW;

	memset(txt, 0, sizeof(txt));
	strncpy(txt, p, char_in_line);	
	gdispGFillStringBox(gw->g.display, gw->g.x, gw->g.y, gw->g.width, ONE_ROW_HEIGHT, txt, gw->g.font, pcol->text, pcol->fill, gJustifyLeft);

	memset(txt, 0, sizeof(txt));
	strncpy(txt, &p[char_in_line], char_in_line);	
	gdispGFillStringBox(gw->g.display, gw->g.x, gw->g.y+ONE_ROW_HEIGHT, gw->g.width, ONE_ROW_HEIGHT, txt, gw->g.font, pcol->text, pcol->fill, gJustifyLeft);

	memset(txt, 0, sizeof(txt));
	strncpy(txt, &p[2*char_in_line], char_in_line);	
	gdispGFillStringBox(gw->g.display, gw->g.x, gw->g.y+2*ONE_ROW_HEIGHT, gw->g.width, ONE_ROW_HEIGHT, txt, gw->g.font, pcol->text, pcol->fill, gJustifyLeft);	

	// Render cursor (if focused)
	if (gwinGetFocus() == (GHandle)gw) {
		// Calculate cursor stuff

		// Draw cursor
		
		tpos += gw->g.x + gdispGetFontMetric(gw->g.font, gFontBaselineX)/2;
		//tpos += gw2obj->cursorPosX;
		gdispGDrawLine(gw->g.display, tpos, gw->g.y + (rowpos*ONE_ROW_HEIGHT), tpos, gw->g.y + ((rowpos+1) * ONE_ROW_HEIGHT), pcol->edge);
	}

	//debug messages
	memset(txt, 0, sizeof(txt));
	sprintf(txt, "PosX=%d: tpos=%d", gw2obj->cursorPosX, tpos);
	gdispGFillStringBox(gw->g.display, gw->g.x, gw->g.y+3*ONE_ROW_HEIGHT, gw->g.width, ONE_ROW_HEIGHT, txt, gw->g.font, pcol->text, pcol->fill, gJustifyLeft);	


	// Render border
	gdispGDrawBox(gw->g.display, gw->g.x, gw->g.y, gw->g.width, gw->g.height, pcol->edge);

	// Render highlighted border if focused
	_gwidgetDrawFocusRect(gw, 0, 0, gw->g.width, gw->g.height);

}

#undef gh2obj
#undef gw2obj

#endif // GFX_USE_GWIN && GWIN_NEED_MULTILINETEXTEDIT
